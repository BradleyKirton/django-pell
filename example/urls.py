from django.urls import path
from django.shortcuts import render
from django import forms
from django_pell.widgets import PellWidget

import pkg_resources


distribution = pkg_resources.get_distribution("django_pell")


INITIAL_TEXT = f"""
<div>
    <h1>Django Pell Widget</h1>
    <p>
        <samp>Project name: {distribution.project_name}</samp>
    </p>
    <p>
        <samp>Platform: {distribution.platform}</samp>
    </p>
    <p>
        <samp>Version: {distribution.version}</samp>
    </p>

    <div>
        If you love Pell please check out the Jared Reich's
        <a href="https://jaredreich.com/pell/"> Blog</a>
    </div>
</div>
"""


class EditorForm(forms.Form):
    """Simple form which exposes the Django Pell Widget."""

    editor = forms.CharField(
        widget=PellWidget(),
        help_text='Pell is "the simplest and smallest WYSIWYG text editor for web, with no dependencies"',
        label=False,
    )


def editor_view(request):
    """Simple view which renders the EditorForm."""

    return render(
        request,
        "editor.html",
        context={"form": EditorForm(initial={"editor": INITIAL_TEXT})},
    )


urlpatterns = [
    path("", editor_view),
]
