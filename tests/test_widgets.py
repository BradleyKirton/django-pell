from django_pell.widgets import PellWidget, PellAction


class TestPellWidget:
    def test___init___defaults(self):
        widget = PellWidget()
        assert widget.actions == PellAction.get_action_list()
        assert widget.default_paragraph_separator == "div"
        assert widget.style_with_css is False

    def test___init___actions(self):
        widget = PellWidget(
            actions=[PellAction.bold, PellAction.italic, PellAction.underline]
        )
        assert widget.actions == [
            PellAction.bold,
            PellAction.italic,
            PellAction.underline,
        ]

    def test___init___paragraph_separator(self):
        widget = PellWidget(default_paragraph_separator="p")
        assert widget.default_paragraph_separator == "p"

    def test___init___style_with_css(self):
        widget = PellWidget(style_with_css=True)
        assert widget.style_with_css is True

    def test_get_context(self):
        widget = PellWidget(
            actions=[
                PellAction.bold.value,
                PellAction.italic.value,
                PellAction.underline.value,
            ]
        )
        context = widget.get_context(name=None, value=None, attrs=None)

        assert "actions" in context["widget"]
        assert "default_paragraph_separator" in context["widget"]
        assert "style_with_css" in context["widget"]
