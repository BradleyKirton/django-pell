from django.apps import AppConfig


class DjangoPellConfig(AppConfig):
    name = "django_pell"
