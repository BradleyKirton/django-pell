from django_pell.fields import PellField
from django_pell.widgets import PellWidget


class TestPellField:
    def test_widget_type(self):
        field = PellField()
        assert isinstance(field.widget, PellWidget)
